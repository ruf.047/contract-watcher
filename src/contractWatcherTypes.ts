import {ABIDefinition, Log, Transaction} from 'web3/types'

export interface AbiDefinitionWithSignature extends ABIDefinition {
    signature: string
}

export interface EthereumParameter {
    key: string,
    value: any
}

export interface TransactionData extends Transaction {
    name: string
    parameters: Array<EthereumParameter>
    receivedAt: number
}

export interface EventData extends Log {
    name: string
    parameters: Array<EthereumParameter>
    receivedAt: number
}

export enum EventType {
    data = 'data'
}
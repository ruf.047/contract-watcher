import * as Web3 from 'web3'
import {AbiDefinitionWithSignature, EventData, EventType, TransactionData} from './contractWatcherTypes'
import {Contract, Log, Subscribe, Transaction} from 'web3/types'
import {promisify} from 'util'

let web3

function getWeb3Instance(nodeIp: string) {
    if (!web3) {
        return web3 = new (Web3 as any)(`ws://${nodeIp}:8546`)
    }
    return web3
}

export class ContractWatcher {
    private web3: Web3.default
    private contract: Contract
    private transactionsSubscriber: Subscribe<Transaction>
    private eventsSubscriber: Subscribe<Log>

    constructor(nodeIp: string, contractAbi: Array<any>, private readonly contractAddress: string) {
        if (!nodeIp || !contractAbi || !contractAddress) {
            throw new Error("Not all parameters passed to constructor")
        }
        this.contractAddress = contractAddress
        this.initializeWatcher(contractAbi, nodeIp, this.contractAddress)
    }

    private initializeWatcher(contractAbi: Array<any>, nodeIp: string, contractAddress: string) {
        this.web3 = getWeb3Instance(nodeIp)
        this.contract = new web3.eth.Contract(contractAbi, contractAddress)
    }

    public async subscribeForTransactionsData(): Promise<void> {
        if (!this.transactionsSubscriber) {
            this.transactionsSubscriber = await this.web3.eth.subscribe('pendingTransactions')
        }
    }

    public async subscribeForEventsData(): Promise<void> {
        if (!this.eventsSubscriber) {
            this.eventsSubscriber = await this.web3.eth.subscribe('logs', {address: this.contractAddress})
        }
    }

    public onTransaction(eventType: EventType, transactionHandler: (transaction: TransactionData) => void): void {
        if (!this.transactionsSubscriber) {
            throw new Error("You have to subscribe to events first")
        }
        if (eventType === EventType.data) {
            this.transactionsSubscriber.on('data', async (data) => {
                const receivedAt = Date.now()
                const transaction = await this.web3.eth.getTransaction(data.toString())
                const contractFunction = (this.contract.options.jsonInterface as Array<AbiDefinitionWithSignature>).find(
                    type => transaction.input.substring(0, 10) === type.signature)
                const decodedInput = this.web3.eth.abi.decodeParameters(contractFunction.inputs, '0x' +
                    transaction.input.substring(10))
                const decodedInputKeys = Object.keys(decodedInput)
                const paramsKeys = decodedInputKeys.filter(
                    inputKey => inputKey !== 'signature' && inputKey !== 'sighash' &&
                        Number.isNaN(parseFloat(inputKey)) && inputKey !== "__length__")
                const functionParams = paramsKeys.map(
                    paramKey => {return {key: paramKey, value: decodedInput[paramKey]}})
                const transactionData: TransactionData = {
                    name: contractFunction.name, ...transaction,
                    parameters: functionParams, receivedAt
                }
                return await transactionHandler(transactionData)
            })
        } else {
            this.transactionsSubscriber.on(eventType, transactionHandler)
        }
    }

    public onEvent(eventType: EventType, blockchainEventHandler: (event: EventData) => void): void {
        if (!this.eventsSubscriber) {
            throw new Error("You have to subscribe to events first")
        }
        if (eventType === EventType.data) {
            this.eventsSubscriber.on('data', async (data: Log) => {
                const receivedAt = Date.now()
                const event = (this.contract.options.jsonInterface as Array<AbiDefinitionWithSignature>).find(
                    type => data.topics.includes(type.signature))
                const inputs = event.inputs.filter(input => !(input.type === "string" && input.indexed))
                const decodedParameters = await this.web3.eth.abi.decodeLog(inputs, data.data, data.topics)
                const decodedEventParamsKeys = Object.keys(decodedParameters)
                const eventParamsKeys = decodedEventParamsKeys.filter(
                    inputKey => Number.isNaN(parseFloat(inputKey)) && inputKey !== "__length__")
                const eventParameters = eventParamsKeys.map(
                    paramKey => {return {key: paramKey, value: decodedParameters[paramKey]}})
                const eventData: EventData = {name: event.name, parameters: eventParameters, receivedAt, ...data}
                return await blockchainEventHandler(eventData)
            })
        }
    }

    public async unsubscribeAll() {
        const unsubscribeEventPromise = promisify((this.eventsSubscriber as any).unsubscribe.bind(this.eventsSubscriber))
        const unsubscribeTransactionsPromise = promisify((this.transactionsSubscriber as any).unsubscribe.bind(this.transactionsSubscriber))
        await unsubscribeEventPromise()
        await unsubscribeTransactionsPromise()
    }
}
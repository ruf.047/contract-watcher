import {expect} from 'chai'
import 'mocha'
import * as Web3 from "web3"
import {contractByteCode, testContractAbi} from './contractData'
import {ContractWatcher} from '../ContractWatcher'
import {EventType} from '../contractWatcherTypes'
import {promisify} from 'util'

describe('Contract Watcher', () => {
    let deployedContract, web3, contractOwnerAddress, contractOwnerPassword = "owner"
    let contractWatcher, setGreetings
    let events = []
    let transactions = []
    const parityHost = process.env.PARITY_HOST || "localhost"
    const httpParityUrl = `http://${parityHost}:8545/`
    const functionParam = "Hello there"

    const prepareContract = async () => {
        contractOwnerAddress = await web3.eth.personal.newAccount(contractOwnerPassword)
        await web3.eth.personal.unlockAccount(contractOwnerAddress, contractOwnerPassword)
        const testContract = new web3.eth.Contract(testContractAbi)
        const deployTransaction = testContract.deploy({data: contractByteCode, arguments: []})
        const estimatedGas = await deployTransaction.estimateGas()
        deployedContract = await deployTransaction.send({gas: estimatedGas, from: contractOwnerAddress})
    }

    before(async () => {
        web3 = new (Web3 as any)(httpParityUrl)
        await prepareContract()
        events = []
        transactions = []
        contractWatcher = new ContractWatcher(parityHost, testContractAbi, deployedContract._address)
        await contractWatcher.subscribeForTransactionsData()
        await contractWatcher.subscribeForEventsData()
        contractWatcher.onEvent(EventType.data, data => {
            events.push(data)
        })
        contractWatcher.onTransaction(EventType.data, data => {
            transactions.push(data)
        })
        await web3.eth.personal.unlockAccount(contractOwnerAddress, contractOwnerPassword)
        setGreetings = await deployedContract.methods.setGreetings(functionParam).
            send({from: contractOwnerAddress})
    })

    it('should deploy contract and execute transaction', async function() {
        expect(setGreetings.events).to.exist
    })
    it('should deploy contract and execute transaction and decode event', function() {
        expect(events.length).to.equal(2)
        const firstEvent = events.find(event => event.name === "TestEvent")
        const secondEvent = events.find(event => event.name === "TestEvent2")
        const firstEventParams = firstEvent.parameters.find(parameterObject => parameterObject.key === 'message')
        const secondEventParams = secondEvent.parameters.find(parameterObject => parameterObject.key === 'message2')
        expect(firstEvent).to.exist
        expect(secondEvent).to.exist
        expect(firstEventParams).to.exist
        expect(secondEventParams).to.exist
        expect(firstEventParams.value).to.equal('0x4A239e3764dDE2bA5265C7551f9a49E0d304530d')
        expect(secondEventParams.value).to.equal(functionParam)
        expect(firstEvent.receivedAt).to.exist
        expect(secondEvent.receivedAt).to.exist
        expect(secondEvent.blockHash).to.exist
        expect(firstEvent.transactionHash).to.exist
    })
    it('should deploy contract and execute transaction and decode function', function() {
        expect(transactions.length).to.equal(1)
        const transaction = transactions[0]
        const paramsObject = transaction.parameters.find(parameterObject => parameterObject.key === '_message')
        expect(transaction).to.exist
        expect(transaction.name).to.equal('setGreetings')
        expect(paramsObject).to.exist
        expect(paramsObject.value).to.equal(functionParam)
        expect(transaction.blockHash).to.exist
        expect(transaction.hash).to.exist
        expect(transaction.receivedAt).to.exist
    })
    it('should unsubscribe', async function() {
        await contractWatcher.unsubscribeAll()
        await web3.eth.personal.unlockAccount(contractOwnerAddress, contractOwnerPassword)
        setGreetings = await deployedContract.methods.setGreetings(functionParam).
            send({from: contractOwnerAddress})
        const promisedTimeout = promisify(setTimeout)

        return await promisedTimeout(1500, () => {
            expect(events.length).to.equal(2)
            expect(transactions.length).to.equal(1)
        })
    })

})
# Contract watcher

[![pipeline status](https://gitlab.com/ruf.047/contract-watcher/badges/master/pipeline.svg)](https://gitlab.com/ruf.047/contract-watcher/commits/master)

Node.js package that allows watching for Ethereum contract transactions and events with minimal overhead and maximum readability.

It listens via WebSockets and decodes function and events with provided contract ABI.

Basically a wrapper on `eth.subscribe` web3.js function


## Installation

To install package execute

```npm i -s contract-watcher```

Then import it into your project

```var watcher = require("contract-watcher")```

or

```import {ContractWatcher} from 'contract-watcher'```


## Demo

You can try the library with your contract, before the installation [here](https://contract-explorer.netlify.com/)


## Usage

To create a new instance of the watcher

- ```new ContractWatcher(nodeIp, abi, contractAddress)```


  `nodeIp:string` - IP of your Ethereum node, with properly configured WebSockets.

  For Parity, make sure port 8546 is open and correct config flags are being run (starting with `--ws`). This package supports Parity versions 1.10 up.

  `abi:Array` -contract to be watched ABI, from the compilation result JSON

  `contractAddres:string` - contract to be watched address

### API:

 -  ```subscribeForTransactionsData() : Promise<void>```

    Subscribe for new pending transactions events

 -  ```contractWatcher.subscribeForEventsData(): Promise<void>```

    Subscribe for new contract events fired

 - ```onTransaction(): void```

    This method wraps web3.js `on()` method on [subscriber](https://web3js.readthedocs.io/en/1.0/web3-eth-subscribe.html#subscribe-pendingtransactions) api for pending transactions.

    On `data` subscription returned object, additionally from web3.js `Transaction` [properties](https://web3js.readthedocs.io/en/1.0/web3-eth.html#gettransaction), contains executed function `name`, array of key value decoded `parameters` and `receivedAt` unix timestamp when it was handled.

    Transaction handler

- ```onEvent(): void```

    This method wraps web3.js `on()` method on [subscriber](https://web3js.readthedocs.io/en/1.0/web3-eth-subscribe.html#subscribe-logs) api for past logs.

    On `data` subscription returned object, additionally from web3.js `Log` [properties](https://web3js.readthedocs.io/en/1.0/web3-eth.html#getpastlogs), contains fired event `name`, array of key value decoded `parameters` and `receivedAt` unix timestamp when it was handled.

- ```unsubscribeAll() : Promise<void>```

    Unsubscribe from all subscriptions


## Limitations

Please note that indexed string parameters of Solidity event will be skipped when decoding.

## License
MIT
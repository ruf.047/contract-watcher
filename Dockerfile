FROM node:8.9.1

ADD package.json package.json
ADD tsconfig.json tsconfig.json

ADD ./src/ /src/

RUN npm install

CMD npm run test


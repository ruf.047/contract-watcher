"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const Web3 = require("web3");
const contractData_1 = require("./contractData");
const ContractWatcher_1 = require("../ContractWatcher");
const contractWatcherTypes_1 = require("../contractWatcherTypes");
const util_1 = require("util");
describe('Contract Watcher', () => {
    let deployedContract, web3, contractOwnerAddress, contractOwnerPassword = "owner";
    let contractWatcher, setGreetings;
    let events = [];
    let transactions = [];
    const parityHost = process.env.PARITY_HOST || "localhost";
    const httpParityUrl = `http://${parityHost}:8545/`;
    const functionParam = "Hello there";
    const prepareContract = () => __awaiter(this, void 0, void 0, function* () {
        contractOwnerAddress = yield web3.eth.personal.newAccount(contractOwnerPassword);
        yield web3.eth.personal.unlockAccount(contractOwnerAddress, contractOwnerPassword);
        const testContract = new web3.eth.Contract(contractData_1.testContractAbi);
        const deployTransaction = testContract.deploy({ data: contractData_1.contractByteCode, arguments: [] });
        const estimatedGas = yield deployTransaction.estimateGas();
        deployedContract = yield deployTransaction.send({ gas: estimatedGas, from: contractOwnerAddress });
    });
    before(() => __awaiter(this, void 0, void 0, function* () {
        web3 = new Web3(httpParityUrl);
        yield prepareContract();
        events = [];
        transactions = [];
        contractWatcher = new ContractWatcher_1.ContractWatcher(parityHost, contractData_1.testContractAbi, deployedContract._address);
        yield contractWatcher.subscribeForTransactionsData();
        yield contractWatcher.subscribeForEventsData();
        contractWatcher.onEvent(contractWatcherTypes_1.EventType.data, data => {
            events.push(data);
        });
        contractWatcher.onTransaction(contractWatcherTypes_1.EventType.data, data => {
            transactions.push(data);
        });
        yield web3.eth.personal.unlockAccount(contractOwnerAddress, contractOwnerPassword);
        setGreetings = yield deployedContract.methods.setGreetings(functionParam).
            send({ from: contractOwnerAddress });
    }));
    it('should deploy contract and execute transaction', function () {
        return __awaiter(this, void 0, void 0, function* () {
            chai_1.expect(setGreetings.events).to.exist;
        });
    });
    it('should deploy contract and execute transaction and decode event', function () {
        chai_1.expect(events.length).to.equal(2);
        const firstEvent = events.find(event => event.name === "TestEvent");
        const secondEvent = events.find(event => event.name === "TestEvent2");
        const firstEventParams = firstEvent.parameters.find(parameterObject => parameterObject.key === 'message');
        const secondEventParams = secondEvent.parameters.find(parameterObject => parameterObject.key === 'message2');
        chai_1.expect(firstEvent).to.exist;
        chai_1.expect(secondEvent).to.exist;
        chai_1.expect(firstEventParams).to.exist;
        chai_1.expect(secondEventParams).to.exist;
        chai_1.expect(firstEventParams.value).to.equal('0x4A239e3764dDE2bA5265C7551f9a49E0d304530d');
        chai_1.expect(secondEventParams.value).to.equal(functionParam);
        chai_1.expect(firstEvent.receivedAt).to.exist;
        chai_1.expect(secondEvent.receivedAt).to.exist;
        chai_1.expect(secondEvent.blockHash).to.exist;
        chai_1.expect(firstEvent.transactionHash).to.exist;
    });
    it('should deploy contract and execute transaction and decode function', function () {
        chai_1.expect(transactions.length).to.equal(1);
        const transaction = transactions[0];
        const paramsObject = transaction.parameters.find(parameterObject => parameterObject.key === '_message');
        chai_1.expect(transaction).to.exist;
        chai_1.expect(transaction.name).to.equal('setGreetings');
        chai_1.expect(paramsObject).to.exist;
        chai_1.expect(paramsObject.value).to.equal(functionParam);
        chai_1.expect(transaction.blockHash).to.exist;
        chai_1.expect(transaction.hash).to.exist;
        chai_1.expect(transaction.receivedAt).to.exist;
    });
    it('should unsubscribe', function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield contractWatcher.unsubscribeAll();
            yield web3.eth.personal.unlockAccount(contractOwnerAddress, contractOwnerPassword);
            setGreetings = yield deployedContract.methods.setGreetings(functionParam).
                send({ from: contractOwnerAddress });
            const promisedTimeout = util_1.promisify(setTimeout);
            return yield promisedTimeout(1500, () => {
                chai_1.expect(events.length).to.equal(2);
                chai_1.expect(transactions.length).to.equal(1);
            });
        });
    });
});

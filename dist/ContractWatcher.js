"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Web3 = require("web3");
const contractWatcherTypes_1 = require("./contractWatcherTypes");
const util_1 = require("util");
let web3;
function getWeb3Instance(nodeIp) {
    if (!web3) {
        return web3 = new Web3(`ws://${nodeIp}:8546`);
    }
    return web3;
}
class ContractWatcher {
    constructor(nodeIp, contractAbi, contractAddress) {
        this.contractAddress = contractAddress;
        if (!nodeIp || !contractAbi || !contractAddress) {
            throw new Error("Not all parameters passed to constructor");
        }
        this.contractAddress = contractAddress;
        this.initializeWatcher(contractAbi, nodeIp, this.contractAddress);
    }
    initializeWatcher(contractAbi, nodeIp, contractAddress) {
        this.web3 = getWeb3Instance(nodeIp);
        this.contract = new web3.eth.Contract(contractAbi, contractAddress);
    }
    subscribeForTransactionsData() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.transactionsSubscriber) {
                this.transactionsSubscriber = yield this.web3.eth.subscribe('pendingTransactions');
            }
        });
    }
    subscribeForEventsData() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.eventsSubscriber) {
                this.eventsSubscriber = yield this.web3.eth.subscribe('logs', { address: this.contractAddress });
            }
        });
    }
    onTransaction(eventType, transactionHandler) {
        if (!this.transactionsSubscriber) {
            throw new Error("You have to subscribe to events first");
        }
        if (eventType === contractWatcherTypes_1.EventType.data) {
            this.transactionsSubscriber.on('data', (data) => __awaiter(this, void 0, void 0, function* () {
                const receivedAt = Date.now();
                const transaction = yield this.web3.eth.getTransaction(data.toString());
                const contractFunction = this.contract.options.jsonInterface.find(type => transaction.input.substring(0, 10) === type.signature);
                const decodedInput = this.web3.eth.abi.decodeParameters(contractFunction.inputs, '0x' +
                    transaction.input.substring(10));
                const decodedInputKeys = Object.keys(decodedInput);
                const paramsKeys = decodedInputKeys.filter(inputKey => inputKey !== 'signature' && inputKey !== 'sighash' &&
                    Number.isNaN(parseFloat(inputKey)) && inputKey !== "__length__");
                const functionParams = paramsKeys.map(paramKey => { return { key: paramKey, value: decodedInput[paramKey] }; });
                const transactionData = Object.assign({ name: contractFunction.name }, transaction, { parameters: functionParams, receivedAt });
                return yield transactionHandler(transactionData);
            }));
        }
        else {
            this.transactionsSubscriber.on(eventType, transactionHandler);
        }
    }
    onEvent(eventType, blockchainEventHandler) {
        if (!this.eventsSubscriber) {
            throw new Error("You have to subscribe to events first");
        }
        if (eventType === contractWatcherTypes_1.EventType.data) {
            this.eventsSubscriber.on('data', (data) => __awaiter(this, void 0, void 0, function* () {
                const receivedAt = Date.now();
                const event = this.contract.options.jsonInterface.find(type => data.topics.includes(type.signature));
                const inputs = event.inputs.filter(input => !(input.type === "string" && input.indexed));
                const decodedParameters = yield this.web3.eth.abi.decodeLog(inputs, data.data, data.topics);
                const decodedEventParamsKeys = Object.keys(decodedParameters);
                const eventParamsKeys = decodedEventParamsKeys.filter(inputKey => Number.isNaN(parseFloat(inputKey)) && inputKey !== "__length__");
                const eventParameters = eventParamsKeys.map(paramKey => { return { key: paramKey, value: decodedParameters[paramKey] }; });
                const eventData = Object.assign({ name: event.name, parameters: eventParameters, receivedAt }, data);
                return yield blockchainEventHandler(eventData);
            }));
        }
    }
    unsubscribeAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const unsubscribeEventPromise = util_1.promisify(this.eventsSubscriber.unsubscribe.bind(this.eventsSubscriber));
            const unsubscribeTransactionsPromise = util_1.promisify(this.transactionsSubscriber.unsubscribe.bind(this.transactionsSubscriber));
            yield unsubscribeEventPromise();
            yield unsubscribeTransactionsPromise();
        });
    }
}
exports.ContractWatcher = ContractWatcher;

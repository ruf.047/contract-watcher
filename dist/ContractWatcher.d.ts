import { EventData, EventType, TransactionData } from './contractWatcherTypes';
export declare class ContractWatcher {
    private readonly contractAddress;
    private web3;
    private contract;
    private transactionsSubscriber;
    private eventsSubscriber;
    constructor(nodeIp: string, contractAbi: Array<any>, contractAddress: string);
    private initializeWatcher;
    subscribeForTransactionsData(): Promise<void>;
    subscribeForEventsData(): Promise<void>;
    onTransaction(eventType: EventType, transactionHandler: (transaction: TransactionData) => void): void;
    onEvent(eventType: EventType, blockchainEventHandler: (event: EventData) => void): void;
    unsubscribeAll(): Promise<void>;
}
